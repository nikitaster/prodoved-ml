from Modules.prediction import PredictionTest

import os

prediction_tester = PredictionTest()

amount_path = os.path.join(os.getcwd(), 'Prediction', 'settings', 'last_object_amount.txt')
model_path = os.path.join(os.getcwd(), 'Prediction', 'models', 'ACTUAL_MODEL.h5')
model_class_path = os.path.join(os.getcwd(), 'Prediction', 'json', 'model_class.json')

dump_amount_path = os.path.join(os.getcwd(), 'Prediction', 'settings', 'dump_last_object_amount.txt')
dump_model_path = os.path.join(os.getcwd(), 'Prediction', 'models', 'dump_ACTUAL_MODEL.h5')
dump_model_class_path = os.path.join(os.getcwd(), 'Prediction', 'json', 'dump_model_class.json')