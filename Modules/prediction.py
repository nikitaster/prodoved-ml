import os

from imageai.Prediction.Custom import ModelTraining, CustomImagePrediction


class Prediction:
    execution_path = os.getcwd()
    train_path = os.path.join(execution_path, 'Prediction', 'train')
    test_path = os.path.join(execution_path, 'Prediction', 'test')
    models_path = os.path.join(execution_path, 'Prediction', 'models')
    json_path = os.path.join(execution_path, 'Prediction', 'json')
    last_object_amount_path = os.path.join(execution_path, 'Prediction', 'settings', 'last_object_amount.txt')

    def __init__(self):
        self.object_amount = 10

    def object_counter(self):
        folders_amount = 0
        for _, dirs, files in os.walk(self.train_path):
            folders_amount += len(dirs)
        return folders_amount

    def set_object_amount(self):
        self.object_amount = self.object_counter()

    def save_object_amount(self):
        with open(self.last_object_amount_path, 'w') as file:
            file.write('{}'.format(self.object_amount))

    def update_object_amount(self):
        with open(self.last_object_amount_path, 'r') as file:
            new_amount = file.read()
            if new_amount.isdigit():
                self.object_amount = new_amount


class PredictionTraining(Prediction):
    def __init__(self):
        super().__init__()
        self.model_trainer = ModelTraining()
        self.model_trainer.setModelTypeAsResNet()
        self.model_trainer.setDataDirectory('Prediction')
        self.num_experiments = 100

    def model_transfer_train(self, old_model_path):
        self.model_trainer.trainModel(num_objects=self.object_amount,
                                      num_experiments=self.num_experiments,
                                      enhance_data=True,
                                      batch_size=32,
                                      show_network_summary=True,
                                      transfer_from_model=old_model_path)

    def model_continue_train(self, old_model_path):
        self.model_trainer.trainModel(num_objects=self.object_amount,
                                      num_experiments=self.num_experiments,
                                      enhance_data=True,
                                      batch_size=32,
                                      show_network_summary=True,
                                      continue_from_model=old_model_path)

    def model_train(self):
        self.model_trainer.trainModel(num_objects=self.object_amount,
                                      num_experiments=self.num_experiments,
                                      enhance_data=True,
                                      batch_size=32,
                                      show_network_summary=True)

    def start(self, arguments):
        self.num_experiments = arguments['n'] or 100
        self.num_experiments = 100 if self.num_experiments < 1 else self.num_experiments

        self.set_object_amount()
        self.save_object_amount()

        if arguments['continue'] and not arguments['transfer']:
            print(os.path.join(self.execution_path, arguments['continue'].name))
            self.model_continue_train(os.path.join(self.execution_path, arguments['continue'].name))
        elif arguments['transfer'] and not arguments['continue']:
            self.model_continue_train(os.path.join(self.execution_path, arguments['transfer'].name))
        else:
            self.model_train()


class PredictionTest(Prediction):
    def __init__(self):
        super().__init__()
        self.prediction = CustomImagePrediction()
        self.prediction.setModelTypeAsResNet()
        self.prediction.setModelPath(os.path.join(self.models_path, 'ACTUAL_MODEL.h5'))
        self.prediction.setJsonPath(os.path.join(self.json_path, "model_class.json"))

    def prepare(self):
        pass

    def start_test(self, arguments):
        if arguments['model']:
            self.prediction.setModelPath(arguments['model'].name)

        self.update_object_amount()
        self.prediction.loadModel(num_objects=self.object_amount)

        files = arguments['files']
        for file in files:
            predictions, probabilities = self.prediction.predictImage(os.path.join(self.execution_path, file.name),
                                                                      result_count=self.object_amount
                                                                      )
            print('Prediction for {}:'.format(file.name))
            for eachPrediction, eachProbability in zip(predictions, probabilities):
                print(eachPrediction, " : ", eachProbability)

    def flask_test(self, image_name):
        self.update_object_amount()
        self.prediction.loadModel(num_objects=self.object_amount)
        predictions, probabilities = self.prediction.predictImage(
            os.path.join(
                self.execution_path, 'testing_images', image_name),
            result_count=1
        )
        if probabilities[0] > 70:
            return predictions[0]
        else:
            return False