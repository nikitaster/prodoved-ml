import os

from imageai.Detection.Custom import DetectionModelTrainer, CustomObjectDetection


class Detection:
    execution_path = os.getcwd()
    train_path = os.path.join(execution_path, 'Detection', 'train')
    test_path = os.path.join(execution_path, 'Detection', 'validate')
    models_path = os.path.join(execution_path, 'Detection', 'models')
    json_path = os.path.join(execution_path, 'Detection', 'json')

    def object_counter(self):
        folders_amount = 0
        for _, dirs, files in os.walk(self.train_path):
            folders_amount += len(dirs)
        return folders_amount


class DetectionTraining(Detection):
    def __init__(self):
        super().__init__()
        self.model_trainer = DetectionModelTrainer()
        self.model_trainer.setModelTypeAsYOLOv3()
        self.model_trainer.setDataDirectory(data_directory="Detection")
        self.num_experiments = 100

    def model_continue_train(self, old_model_path):
        self.model_trainer.setTrainConfig(object_names_array=['milk', 'cola'],
                                          batch_size=4,
                                          num_experiments=self.num_experiments,
                                          train_from_pretrained_model=old_model_path
                                          )
        self.model_trainer.trainModel()

    def model_train(self):
        self.model_trainer.setTrainConfig(object_names_array=['milk', 'cola'],
                                          batch_size=4,
                                          num_experiments=self.num_experiments
                                          )
        self.model_trainer.trainModel()

    def start(self, arguments):
        self.num_experiments = arguments['n'] or 100
        self.num_experiments = 100 if self.num_experiments < 1 else self.num_experiments

        if arguments['continue']:
            self.model_continue_train(self.execution_path + '/{}'.format(arguments['continue'].name))
        else:
            self.model_train()


class DetectionTest(Detection):
    def __init__(self):
        super().__init__()
        self.detector = CustomObjectDetection()
        self.detector.setModelTypeAsYOLOv3()
        self.detector.setModelPath(os.path.join(self.models_path, 'hololens-ex-60--loss-2.76.h5'))
        self.detector.setJsonPath(os.path.join(self.json_path, 'detection_config.json'))

    def prepare(self):
        pass

    def start_test(self, arguments):
        if arguments['model']:
            self.detector.setModelPath(arguments['model'].name)
        self.detector.loadModel()

        files = arguments['files']
        for file in files:
            detections = self.detector.detectObjectsFromImage(input_image=file.name)

            # detections = self.detector.detectObjectsFromImage(input_image=file.name,
            #                                                   output_image_path="holo1-detected.jpg")

            for detection in detections:
                print('Detection for {}:'.format(file.name))
                print(detection["name"], " : ", detection["percentage_probability"], " : ", detection["box_points"])