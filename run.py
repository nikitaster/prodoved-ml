from parsers.detection.detection_test import detection_test_file
from parsers.detection.detection_train import detection_train_file
from parsers.prediction.prediction_test import prediction_test_file
from parsers.prediction.prediction_train import prediction_train_file
from parsers.main.start import start


if __name__ == '__main__':
    parser, subparsers = start()

    # =======PREDICTION=======#
    prediction_train_file(subparsers)
    prediction_test_file(subparsers)

    # ========DETECTION=========#
    detection_train_file(subparsers)
    detection_test_file(subparsers)

    args = parser.parse_args()

    if not vars(args):
        parser.print_help()
    else:
        args.func(vars(args))
