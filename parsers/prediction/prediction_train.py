import argparse

from Modules.prediction import PredictionTraining


def prediction_train(arguments):
    trainer = PredictionTraining()
    trainer.start(arguments)


def prediction_train_file(subparsers):
    parser_prediction_train = subparsers.add_parser('prediction-train', help='it is train method')
    parser_prediction_train.set_defaults(func=prediction_train)
    parser_prediction_train.add_argument('--continue', metavar='filename', type=argparse.FileType('r'),
                                         help='Continue training model')
    parser_prediction_train.add_argument('--transfer', metavar='filename', type=argparse.FileType('r'),
                                         help='Retrain model with new data')
    parser_prediction_train.add_argument('-n', metavar='num of experiments', type=int,
                                             help='num of experiments')
