import argparse

from Modules.prediction import PredictionTest


def prediction_test(arguments):
    tester = PredictionTest()
    tester.start_test(arguments)


def prediction_test_file(subparsers):
    parser_prediction_test = subparsers.add_parser('prediction-test', help='it is test method')
    parser_prediction_test.add_argument('files', type=argparse.FileType('r'), nargs='+')
    parser_prediction_test.add_argument('--model', metavar='filename', type=argparse.FileType('r'),
                                        help='Model')
    parser_prediction_test.set_defaults(func=prediction_test)
