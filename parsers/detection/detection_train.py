import argparse

from Modules.detection import DetectionTraining


def detection_train(arguments):
    trainer = DetectionTraining()
    trainer.start(arguments)


def detection_train_file(subparsers):
    parser_detection_train = subparsers.add_parser('detection-train', help='it is train method')
    parser_detection_train.set_defaults(func=detection_train)
    parser_detection_train.add_argument('--continue', metavar='filename', type=argparse.FileType('r'),
                                        help='Continue training model')
    parser_detection_train.add_argument('-n', metavar='num of experiments', type=int,
                                        help='num of experiments')
