import argparse

from Modules.detection import DetectionTest


def detection_test(arguments):
    tester = DetectionTest()
    tester.start_test(arguments)


def detection_test_file(subparsers):
    parser_detection_test = subparsers.add_parser('detection-test', help='it is test method')
    parser_detection_test.add_argument('files', type=argparse.FileType('r'), nargs='+')
    parser_detection_test.add_argument('--model', metavar='filename', type=argparse.FileType('r'),
                                        help='Model')
    parser_detection_test.set_defaults(func=detection_test)
