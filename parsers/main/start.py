import argparse


def start():
    parser = argparse.ArgumentParser(
        usage='Script options "%(prog)s"',
        description='The "%(prog)s" is a test script',
        epilog='Sample: "%(prog)s -h" - view help'
    )
    subparsers = parser.add_subparsers()

    return parser, subparsers
