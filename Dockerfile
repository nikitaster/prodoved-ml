FROM python:3.7

ENV PYTHONUNBUFFERED 1
RUN mkdir /ai
WORKDIR /ai
COPY requirements.txt /ai/
RUN pip3 install -r requirements.txt

COPY . /ai/

