# Инструкция для использования проекта

## Навигация
<details>
<summary>Общая структура проекта</summary>

[Общая структура проекта](#structure)

</details>

<details>
<summary>Первоначальная настройка</summary>

[Первоначальная настройка](#setup)

</details>

<details>
<summary>Распознование Detection</summary>

[Распознование Detection](#detection)

[Тренировка модели Detection](#detection-train)

[Параметры для запуска Detection](#params-train-detection)

[Тестирование модели Detection](#detection-test)

[Параметры для тестирования Detection](#params-test-detection)

</details>

<details>
<summary>Распознование Prediction</summary>

[Распознование Prediction](#prediction)

[Тренировка модели Prediction](#prediction-train)

[Параметры для запуска Prediction](#params-train-prediction)

[Тестирование модели Prediction](#prediction-test)

[Параметры для тестирования Prediction](#params-test-prediction)

</details>

## <a name="structure"></a>Общая структура проекта
>>> 
Наш проект построен на основе библиотеки [ImageAI](https://github.com/OlafenwaMoses/ImageAI)

Вы можете натренировать модель на две задачи: Prediction и Detection
<details>
<summary> Информация о Detection</summary>
Все обученные для Detection модели хранятся в папке [models](#), а подробрая информация в [json](#).

В папке [settings](#) находится файл, записывающий, сколько тренировок было в предыдущий раз 
</details>

<details>
<summary> Информация о Prediction</summary>
Все обученные модели для Prediction хранятся в папке [models](Prediction/models), а подробная информация в [json](hPrediction/json).

В папке [settings](Prediction/settings) находится файл, записывающий, сколько тренировок было в предыдущий раз
</details>

>>>

# <a name="setup"></a>Первоначальная настройка
>>>
Через консоль открыть пустую директори. и выполнить команды:
```
git init
git clone https://gitlab.informatics.ru/2019-2020/online/s101/scanner-ml.git
```
После завершения клонирования проекта откройте его в любой удобной вам IDE, поддерживающей Python
Далее выполните комманду 
```
pip upgrade --pip
pip install -r requirements.txt
```
Дождитесь окончания загрузки всех необходимых библиотек.
Теперь проект готов к использованию
>>>

# <a name="detection"></a>Распознование Detection
>>>
# <a name="detection-train"></a>Тренировка модели Detection
Этот вид распознования направлен на нахождение объектов на фото или видео

Для начала тренировки в директории Detection нужно создать папки train и validation

Они предназначены для хранения dataset для тренировки модели.
Общее дерево этих дирректорий выглядит так
<details>
<summary>Нажмите, чтобы увидеть дерево папок</summary>

```
>> train    >> images       >> img_1.jpg  (shows Object_1)
            >> images       >> img_2.jpg  (shows Object_2)
            >> images       >> img_3.jpg  (shows Object_1, Object_3 and Object_n)
            >> annotations  >> img_1.xml  (describes Object_1)
            >> annotations  >> img_2.xml  (describes Object_2)
            >> annotations  >> img_3.xml  (describes Object_1, Object_3 and Object_n)

>> validation   >> images       >> img_151.jpg (shows Object_1, Object_3 and Object_n)
                >> images       >> img_152.jpg (shows Object_2)
                >> images       >> img_153.jpg (shows Object_1)
                >> annotations  >> img_151.xml (describes Object_1, Object_3 and Object_n)
                >> annotations  >> img_152.xml (describes Object_2)
                >> annotations  >> img_153.xml (describes Object_1)
```

Стандартный вид дерева папок
![Image not found :(](img/detection_folder.png "Detection folder")

</details>
Для работы необходимо создать аннотации для каждой картинки. Это можно сделать, например, с помощью [LabelIMG](https://github.com/tzutalin/labelImg)

Минимальный размер dataset для тренировки 200 картинок, но для большей точности рекоммендуется брать около 500 различных фото каждого объекта.

Для правильной тренировки в директории train должно нахоится 75-80% dataset'а(картинки и аннотации к ним), а в validdation должны быть остальные 25-20% dataset

***Картинки(и аннотации) нельзя дублировать!***

Все аннотации должны быть в формате .xml

Далее в консоли нужно запустить команду ```python run.py detection-train <params>```

<details>
<summary><a name="params-train-detection"></a>Параметры для запуска тренировки </summary>

```
-n #Число экспериментов(эпох) для тренировки модели(по умолчанию 100). После параметра указывается целое положительное число

--continue #Продолжение тренировки уже существующей detection-модели. После параметра указывается путь до модели + имя модели <model_name>.h5

-h #Вывод информации о доступных командах
```

</details>

# <a name="detection-test"></a>Тестирование модели
Для тестирования модели detection должна иметься уже натренированная модель

Для запуска тестирования необходимо запустить команду ``` python run.py detection-test <image_for_test>.png <params>```
### <a name="params-test-detection"></a>Параметр для запуска тестирования
```
--model #Выбор модели для тестирования. После параметра указывается путь до модели + имя модели <model_name>.h5
```
>>>

# <a name="prediction"></a>Распознование Prediction
>>>
# <a name="prediction-train"></a>Тренировка модели
В этом виде распознования модель выдаёт процентное соотношение совпадение объекта на фото с объектом из нейросети

Для начала тренировки необходимо создать в директории Prediction две папки: train и test. Внутри этих папок создаются отдельные папки для каждого объекта
<details>
<summary style="color: #000">Нажмите, чтобы увидеть дерево папок</summary>

```
train >> dog >> dog-train-images
         cat >> cat-train-images
test >> dog >> dog-test-images
        cat >> cat-test-images
```

Стандартный вид дерева папок
![Image not found :(](img/prediction_folder.png "Prediction folder")

</details>
Обязательными условиями для тренировки является как минимум два объекта для распознания, картинки для тренировки и тестов не должны совпадать, а также количество папок в директории train должно быть равно количеству папок в test, названия папок должны быть одинаковыми

Минимальное число картинок каждого объекта для тренировки - 100 фото в dataset. Для наилучшего результата рекомендуется использовать не менее 200 фото одного объекта.

Для корректного обучения в train необходимо добавить 80% dataset'a, а в test оставшиеся 20%. ***Фото не должны дублироваться!***

После всех приготовлений необходимо написать в консоли ```python run.py prediction-train <params>```

<details>
<summary><a name="params-train-prediction"></a>Параметры для запуска тренировки</summary>

```
-n #Число экспериментов(эпох) для тренировки модели(по умолчанию 100). После параметра указывается целое положительное число

--continue #Продолжение обучения модели. После параметра указывается путь до модели + имя модели <model_name>.h5

--transfer #Переобучение модели на новом dataset. После параметра указывается путь до модели + имя модели <model_name>.h5
```

</details>

# <a name="prediction-test"></a>Тестирование модели
Для тестирования модели prediction должна иметься уже натренированная модель

Для запуска тестирования необходимо запустить команду ``` python run.py prediction-test <image_for_test>.png <params>```
### <a name="params-test-prediction"></a>Параметр для запуска тестирования
```
--model #Выбор модели для тестирования. После параметра указывается путь до модели + имя модели <model_name>.h5
```
>>>
