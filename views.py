from flask import Flask, request, jsonify, render_template

from werkzeug.utils import secure_filename
from PIL import Image
import os

from shutil import copyfile

from Modules.prediction import *
from settings import *

app = Flask(__name__)

# добавим ограничение на размер файлов в 300 мегабайт
app.config['MAX_CONTENT_LENGTH'] = 300 * 1024 * 1024


@app.route('/', methods=['post', 'get'])
def index():
    if request.method == 'POST':
        return 'POST'
    elif request.method == 'GET':
        # Получение картинки
        image = request.files.get('image')

        if image:

            # СОХРАНЕНИЕ КАРТИНКИ ПОЛЬЗОВАТЕЛЯ ДЛЯ ОБРАБОТКИ
            nom_image = secure_filename(image.filename)
            image = Image.open(image)
            image_path = os.path.join(os.getcwd(), 'testing_images', nom_image)
            image.save(image_path)

            # Обработать картинки, записать результат
            result = prediction_tester.flask_test(nom_image)

            # Удалить картинку
            os.remove(image_path, dir_fd=None)

            if result:
                response = {'model_name': result}
                return jsonify(response)

        return jsonify()


@app.route('/update', methods=['post', 'get'])
def update():
    if request.method == 'POST':
        new_objects_amount = request.form.get('objects_amount')
        new_model_file = request.files.get('model')
        new_model_class = request.files.get('model_class')

        # делаем дамп текущих настроек
        copyfile(amount_path, dump_amount_path)
        copyfile(model_path, dump_model_path)
        copyfile(model_class_path, dump_model_class_path)

        # Обновляем количество объектов в модели
        with open(amount_path, 'w') as file:
            file.write('{}'.format(new_objects_amount))

        # обновляем файл модели
        new_model_file.save(model_path)

        # обновляем файл объектов в модели
        new_model_class.save(model_class_path)

        try:
            # пересоздаем объект-тестировщик
            prediction_tester = PredictionTest()
            # тестируем
            prediction_tester.flask_test(os.path.join(os.getcwd(), 'first_test_image.jpg'))
            return render_template('update.html', status_ok=True)
        except Exception as exc:
            # возвращаем настройки из дампа
            copyfile(dump_amount_path, amount_path)
            copyfile(dump_model_path, model_path)
            copyfile(dump_model_class_path, model_class_path)
            print(exc)
            return render_template('update.html', status_error=True, error=exc)

    return render_template('update.html')


if __name__ == "__main__":
    app.run(host='0.0.0.0', port='5000')